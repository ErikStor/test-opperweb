<?php

namespace App\Http\Controllers;

use App\Models\Posts;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PostsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index()
    {
        try {

            $posts = Posts::with(
                [
                    'categorias:id,nombre',
                    'comentarios:posts_id,contenido'
                ])
                ->select(
                    [
                        'id',
                        'titulo',
                        'contenido',
                        'created_at',
                        'categorias_id'
                    ]
                )->get();

            return response()->json($posts);
        } catch (\Exception $exception) {

            return response()->json(
                [
                    'msg' => 'Ocurrió un error inesperado',
                    'error_msg' => $exception->getMessage()
                ],
                500
            );
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {

        $request->validate([
            'titulo' => 'required|min:3|max:150',
            'contenido' => 'required|min:3|max:65535',
            'categorias_id' => 'required|integer|exists:categorias,id'
        ]);

        try {

            DB::beginTransaction();

            Posts::create($request->all());

            DB::commit();

            return response()->json([
                'msg' => 'Se registró el post exitosamente'
            ]);
        } catch (\Exception $exception) {

            DB::rollBack();

            error_log($exception->getMessage());

            return response()->json(
                [
                    'msg' => 'Ocurrió un error inesperado',
                    'error_msg' => $exception->getMessage()
                ],
                $exception->getCode()
            );
        }

    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function show(int $id)
    {
        try {

            if (!is_numeric($id)) {
                throw new \Exception('El identificador del post no tiene el formato correcto', 400);
            }

            $post = Posts::with(
                [
                    'categorias:id,nombre',
                    'comentarios:posts_id,contenido'
                ])
                ->select(
                    [
                        'id',
                        'titulo',
                        'contenido',
                        'created_at',
                        'categorias_id'
                    ]
                )->find($id);

            if (!$post) {
                throw new \Exception('El post solicitado no fue encontrado', 404);
            }

            return response()->json($post);
        } catch (\Exception $exception) {
            error_log($exception->getMessage());

            return response()->json(
                [
                    'msg' => ($exception->getCode() === 500) ? 'Ocurrió un error inesperado' : $exception->getMessage(),
                    'error_msg' => $exception->getMessage()
                ],
                $exception->getCode()
            );
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function edit(int $id)
    {
        try {

            if (!is_numeric($id)) {
                throw new \Exception('El identificador del post no tiene el formato correcto', 400);
            }

            $post = Posts::with(
                [
                    'categorias:id,nombre',
                    'comentarios:posts_id,contenido'
                ])
                ->select(
                    [
                        'id',
                        'titulo',
                        'contenido',
                        'created_at',
                        'categorias_id'
                    ]
                )->find($id);

            if (!$post) {
                throw new \Exception('El post solicitado no fue encontrado', 404);
            }

            return response()->json($post);
        } catch (\Exception $exception) {
            error_log($exception->getMessage());

            return response()->json(
                [
                    'msg' => ($exception->getCode() === 500) ? 'Ocurrió un error inesperado' : $exception->getMessage(),
                    'error_msg' => $exception->getMessage()
                ],
                $exception->getCode()
            );
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     */
    public function update(Request $request, int $id)
    {

        try {


            $request->validate([
                'titulo' => 'required|min:3|max:150',
                'contenido' => 'required|min:3|max:65535',
                'categorias_id' => 'required|integer|exists:categorias,id'
            ]);

            if (!is_numeric($id)) {
                throw new \Exception('El identificador del post no tiene el formato correcto', 400);
            }

            DB::beginTransaction();

            $post = Posts::find($id);

            if (!$post) {
                throw new \Exception('El post solicitado no pudo ser encontrado', 404);
            }

            $post->update($request->all());

            DB::commit();

            return response()->json(
                [
                    'msg' => 'Se actualizó el post exitosamente'
                ]
            );
        } catch (\Exception $exception) {

            DB::rollBack();

            error_log($exception->getMessage());

            return response()->json(
                [
                    'msg' => 'Ocurrió un error inesperado',
                    'error_msg' => $exception->getMessage()
                ],
                $exception->getCode()
            );
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function destroy(int $id)
    {
        try {

            if (!is_numeric($id)) {
                throw new \Exception('El identificador del post no tiene el formato correcto', 400);
            }

            DB::beginTransaction();

            $post = Posts::find($id);

            if (!is_numeric($id) || !$post) {
                throw new \Exception('El post solicitado no pudo ser encontrado', 404);
            }

            $post->delete();

            DB::commit();

            return response()->json(
                [
                    'msg' => 'Se eliminó el post exitosamente'
                ]
            );
        } catch (\Exception $exception) {

            DB::rollBack();

            error_log($exception->getMessage());

            return response()->json(
                [
                    'msg' => 'Ocurrió un error inesperado',
                    'error_msg' => $exception->getMessage()
                ],
                $exception->getCode()
            );
        }
    }


}
