<?php

namespace App\Http\Controllers;

use App\Models\Comentarios;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ComentariosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index()
    {
        try {

            $comentarios = Comentarios::with(
                [
                    'posts:id,titulo'
                ])
                ->select(
                    [
                        'id',
                        'contenido',
                        'created_at',
                        'posts_id'
                    ]
                )->get();

            return response()->json($comentarios);
        } catch (\Exception $exception) {

            return response()->json(
                [
                    'msg' => 'Ocurrió un error inesperado',
                    'error_msg' => $exception->getMessage()
                ],
                500
            );
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {

        $request->validate([
            'contenido' => 'required|min:3|max:500',
            'posts_id' => 'required|integer|exists:posts,id'
        ]);

        try {

            DB::beginTransaction();

            Comentarios::create($request->all());

            DB::commit();

            return response()->json([
                'msg' => 'Se registró el comentario exitosamente'
            ]);
        } catch (\Exception $exception) {

            DB::rollBack();

            error_log($exception->getMessage());

            return response()->json(
                [
                    'msg' => 'Ocurrió un error inesperado',
                    'error_msg' => $exception->getMessage()
                ],
                $exception->getCode()
            );
        }

    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function show(int $id)
    {
        try {

            if (!is_numeric($id)) {
                throw new \Exception('El identificador del comentario no tiene el formato correcto', 400);
            }

            $comentario = Comentarios::with(
                [
                    'posts:id,titulo'
                ])
                ->select(
                    [
                        'contenido',
                        'posts_id',
                        'created_at'
                    ]
                )->find($id);

            if (!$comentario) {
                throw new \Exception('El comentario solicitado no fue encontrado', 404);
            }

            return response()->json($comentario);
        } catch (\Exception $exception) {
            error_log($exception->getMessage());

            return response()->json(
                [
                    'msg' => ($exception->getCode() === 500) ? 'Ocurrió un error inesperado' : $exception->getMessage(),
                    'error_msg' => $exception->getMessage()
                ],
                $exception->getCode()
            );
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function edit(int $id)
    {
        try {


            if (!is_numeric($id)) {
                throw new \Exception('El identificador del comentario no tiene el formato correcto', 400);
            }

            $comentario = Comentarios::with(
                [
                    'posts:id,titulo'
                ])
                ->select(
                    [
                        'contenido',
                        'posts_id',
                        'created_at'
                    ]
                )->find($id);

            if (!$comentario) {
                throw new \Exception('El comentario solicitado no fue encontrado', 404);
            }

            return response()->json($comentario);
        } catch (\Exception $exception) {
            error_log($exception->getMessage());

            return response()->json(
                [
                    'msg' => ($exception->getCode() === 500) ? 'Ocurrió un error inesperado' : $exception->getMessage(),
                    'error_msg' => $exception->getMessage()
                ],
                $exception->getCode()
            );
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     */
    public function update(Request $request, int $id)
    {

        try {

            $request->validate(
                [
                    'contenido' => 'required|min:3|max:500',
                    'posts_id' => 'required|integer|exists:categorias,id'
                ]
            );

            if (!is_numeric($id)) {
                throw new \Exception('El identificador del comentario no tiene el formato correcto', 400);
            }

            DB::beginTransaction();

            $comentario = Comentarios::find($id);

            if (!$comentario) {
                throw new \Exception('El cometario solicitado no pudo ser encontrado', 404);
            }

            $comentario->update($request->all());

            DB::commit();

            return response()->json(
                [
                    'msg' => 'Se actualizó el comentario exitosamente'
                ]
            );
        } catch (\Exception $exception) {

            DB::rollBack();

            error_log($exception->getMessage());

            return response()->json(
                [
                    'msg' => 'Ocurrió un error inesperado',
                    'error_msg' => $exception->getMessage()
                ],
                $exception->getCode()
            );
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function destroy(int $id)
    {
        try {

            if (!is_numeric($id)) {
                throw new \Exception('El identificador del comentario no tiene el formato correcto', 400);
            }

            DB::beginTransaction();

            $post = Comentarios::find($id);

            if (!is_numeric($id) || !$post) {
                throw new \Exception('El comentario solicitado no pudo ser encontrado', 404);
            }

            $post->delete();

            DB::commit();

            return response()->json(
                [
                    'msg' => 'Se eliminó el comentario exitosamente'
                ]
            );
        } catch (\Exception $exception) {

            DB::rollBack();

            error_log($exception->getMessage());

            return response()->json(
                [
                    'msg' => 'Ocurrió un error inesperado',
                    'error_msg' => $exception->getMessage()
                ],
                $exception->getCode()
            );
        }
    }


}
