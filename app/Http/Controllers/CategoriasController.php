<?php

namespace App\Http\Controllers;

use App\Http\Requests\categorias\StoreCategoriasRequest;
use App\Models\Categorias;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CategoriasController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        try {
            return response()->json(Categorias::all(['id', 'nombre']));
        } catch (\Exception $exception) {
            return response()->json(
                [
                    'msg' => 'Ocurrió un error inesperado',
                    'error_msg' => $exception->getMessage()
                ],
                $exception->getCode()
            );
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {

        $request->validate([
            'nombre' => 'required|min:3|max:255'
        ]);

        try {

            DB::beginTransaction();

            Categorias::create($request->all());

            DB::commit();

            return response()->json([
                'msg' => 'Se registró la categoria exitosamente'
            ]);
        } catch (\Exception $exception) {

            DB::rollBack();

            error_log($exception->getMessage());

            return response()->json(
                [
                    'msg' => 'Ocurrió un error inesperado',
                    'error_msg' => $exception->getMessage()
                ],
                $exception->getCode()
            );
        }

    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        try {

            if (!is_numeric($id)) {
                throw new \Exception('El identificador de la categoria no tiene el formato correcto', 400);
            }

            $categoria = Categorias::find($id);

            if (!$categoria) {
                throw new \Exception('La categoria solicitada no fue encontrada', 404);
            }

            return response()->json(Categorias::select(['nombre'])->find($id));
        } catch (\Exception $exception) {
            error_log($exception->getMessage());

            return response()->json(
                [
                    'msg' => ($exception->getCode() === 500) ? 'Ocurrió un error inesperado' : $exception->getMessage(),
                    'error_msg' => $exception->getMessage()
                ],
                $exception->getCode()
            );
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit($id)
    {
        try {

            if (!is_numeric($id)) {
                throw new \Exception('El identificador de la categoria no tiene el formato correcto', 400);
            }

            $categoria = Categorias::find($id);

            if (!$categoria) {
                throw new \Exception('La categoria solicitada no fue encontrada', 404);
            }

            return response()->json(Categorias::select(['id', 'nombre'])->find($id));
        } catch (\Exception $exception) {
            error_log($exception->getMessage());

            return response()->json(
                [
                    'msg' => ($exception->getCode() === 500) ? 'Ocurrió un error inesperado' : $exception->getMessage(),
                    'error_msg' => $exception->getMessage()
                ],
                $exception->getCode()
            );
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {

        try {


            $request->validate([
                'nombre' => 'required|min:3|max:255'
            ]);

            DB::beginTransaction();

            $categoria = Categorias::find($id);

            if (!is_numeric($id) || !$categoria) {
                throw new \Exception('La categoria solicitada no fue encontrada', 404);
            }

            $categoria->update($request->all());

            DB::commit();

            return response()->json(
                [
                    'msg' => 'Se actualizó la categoria exitosamente'
                ]
            );
        } catch (\Exception $exception) {

            DB::rollBack();

            error_log($exception->getMessage());

            return response()->json(
                [
                    'msg' => 'Ocurrió un error inesperado',
                    'error_msg' => $exception->getMessage()
                ],
                $exception->getCode()
            );
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        try {

            DB::beginTransaction();

            $categoria = Categorias::find($id);

            if (!is_numeric($id) || !$categoria) {
                throw new \Exception('La categoria solicitada no fue encontrada', 404);
            }

            $categoria->delete();

            DB::commit();

            return response()->json(
                [
                    'msg' => 'Se eliminó la categoria exitosamente'
                ]
            );
        } catch (\Exception $exception) {

            DB::rollBack();

            error_log($exception->getMessage());

            return response()->json(
                [
                    'msg' => 'Ocurrió un error inesperado',
                    'error_msg' => $exception->getMessage()
                ],
                $exception->getCode()
            );
        }
    }


}
