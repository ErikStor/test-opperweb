<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::resource('/categorias', \App\Http\Controllers\CategoriasController::class, ['except' => ['create']]);


Route::resource('/posts', \App\Http\Controllers\PostsController::class, ['except' => ['create']]);

Route::resource('/comentarios', \App\Http\Controllers\ComentariosController::class, ['except' => ['create']]);
