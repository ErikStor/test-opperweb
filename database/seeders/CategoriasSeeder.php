<?php

namespace Database\Seeders;

use App\Models\Categorias;
use App\Models\Comentarios;
use App\Models\Posts;
use Illuminate\Database\Seeder;

class CategoriasSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Categorias::factory()
            ->count(10)
            ->hasPosts(1)
            ->create();
    }
}
