<?php

namespace Database\Factories;

use App\Models\Posts;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Posts>
 */
class PostsFactory extends Factory
{

    protected $model = Posts::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'categorias_id' => rand(1, 10),
            'titulo' => $this->faker->realText(150),
            'contenido' => $this->faker->text(),
            'created_at' => now(),
            'updated_at' => now(),
        ];
    }
}
