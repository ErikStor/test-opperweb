<?php

namespace Database\Factories;

use App\Models\Comentarios;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Comentarios>
 */
class ComentariosFactory extends Factory
{

    protected $model = Comentarios::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'posts_id' => rand(0, 10),
            'contenido' => $this->faker->text(),
            'created_at' => now(),
            'updated_at' => now(),
        ];
    }
}
